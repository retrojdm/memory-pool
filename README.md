# Arduino Memory Pool library.

A simple implementation of a memory allocater to use static instead of dynamic memory.

Does not throw exceptions, since Arduino is compiled with the `-fno-exceptions` option. Instead, bad allocations simply return `nullptr`.

Based on the [Memory Pool in C++ tutorial](http://www.mario-konrad.ch/blog/programming/cpp-memory_pool.html) by Mario Konrad.

andrewzuku, retrojdm.com