#pragma once

#if defined(ARDUINO)
#include <Arduino.h>
#else
#include <cstdint>
#include <cstddef>
#endif

#include <MemoryPool.h>
#include <StrategyLinear.h> // The Linear strategy uses an array of blocks of memory.

class TestClass
{
public:
  static const size_t MEMORY_BLOCK_SIZE = 8; // bytes
  static const size_t MEMORY_POOL_SIZE = 5;

  TestClass(int x, int y) : x(x), y(y) {}
  virtual ~TestClass() {}

  int x;
  int y;

  // operator 'new' and 'delete' overloaded to redirect any memory management,
  // in this case delegated to the memory pool.
  // Based on the Memory Pool in C++ tutorial by Mario Konrad.
  // See http://www.mario-konrad.ch/blog/programming/cpp-memory_pool.html
  void * operator new(unsigned int n) { return memoryPool.alloc(n); }
  void* operator new(size_t size, const std::nothrow_t& tag) noexcept { return memoryPool.alloc(size); }
  void operator delete(void * p) { memoryPool.free(p); }

// Definition of the static memory pool.
private:
  using TestClassMemoryPool = MemoryPool<MEMORY_BLOCK_SIZE, StrategyLinear<MEMORY_BLOCK_SIZE, MEMORY_POOL_SIZE>>;
  static TestClassMemoryPool memoryPool;
};

// Instance of the static memory pool.
TestClass::TestClassMemoryPool TestClass::memoryPool;