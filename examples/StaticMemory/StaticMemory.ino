#if defined(ARDUINO)
#include <Arduino.h>
#else
#include <cstdint>
#include <cstddef>
#endif

#include <new>
#include "TestClass.h"

void setup()
{
  // See TestClass::MEMORY_POOL_SIZE
  Serial.println("Static memory pool test.");
  Serial.println();
  Serial.println("Test class configured with memory pool for 5 objects.");
  Serial.println("Attempting to create 6 objects...");

  const size_t TRY_TO_ALLOCATE = 6;

  TestClass * objects[TRY_TO_ALLOCATE];
  for (uint8_t i = 0; i < TRY_TO_ALLOCATE; i++)
  {
    objects[i] = new(std::nothrow) TestClass(i * 2, i * 3);

    Serial.println();
    Serial.print("objects[");
    Serial.print(i);
    Serial.print("]: ");
    if (objects[i])
    {
      Serial.print(" (");
      Serial.print(objects[i]->x);
      Serial.print(", ");
      Serial.print(objects[i]->y);
      Serial.println(")");
    }
    else
    {
      Serial.println("nullptr");
    }
  }

  for (uint8_t i = 0; i < TRY_TO_ALLOCATE; i++)
  {
    delete objects[i];
  }

  Serial.println();
  Serial.println("Done.");
}

void loop()
{
}