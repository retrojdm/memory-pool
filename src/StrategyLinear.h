#pragma once

#if defined(ARDUINO)
#include <Arduino.h>
#else
#include <cstdint>
#include <cstddef>
#endif

template<size_t BlockSize, size_t MaxBlocks>
class StrategyLinear
{
private:
  uint8_t buf[BlockSize * MaxBlocks];
  bool state[MaxBlocks]; // true if the memory chunk is occupied, false if it is free

public:
  void init()
  {
    // initially all memory chunks are free
    for (size_t i = 0; i < MaxBlocks; i++)
    {
      state[i] = false;
    }
  }

  void* allocate()
  {
    // search linearily through the array to find an unused
    // memory chunk, take it and mark it occupied
    for (size_t i = 0; i < MaxBlocks; i++)
    {
      if (!state[i])
      {
        state[i] = true;
        return &buf[BlockSize * i];
      }
    }

    // no free memory chunks could be found
    return nullptr;
  }

  void deallocate(void *p)
  {
    // search all memory chunks to find the one to be freed,
    // then mark it as not occupied.
    // please note: this could have been done with pointer arithmetic,
    // to gain more efficiency, but is not done here intentionally.
    for (size_t i = 0; i < MaxBlocks; i++)
    {
      if (&buf[BlockSize * i] == p)
      {
        state[i] = false;
        return;
      }
    }
  }

// We need to align the data to 8-byte boundaries to avoid crashing the teensy when trying to access time_t types etc.
} __attribute__((aligned(8)));