#pragma once

#if defined(ARDUINO)
#include <Arduino.h>
#else
#include <iostream>
#include <cstdint>
#include <cstddef>
#endif

template<size_t BlockSize, class Strategy>
class MemoryPool
{
private:
  Strategy s;

public:
  MemoryPool()
  {
    s.init();
  }

  void* alloc(size_t bytes)
  {
    if (bytes > BlockSize)
    {
      return nullptr;
    }

    return s.allocate();
  }

  void free(void *p)
  {
    s.deallocate(p);
  }
};